Castle Game Engine Asset Store dummy, mostly experimental stuff to try things out, but, of course, you can use everything here :)

https://eugeneloza.gitlab.io/castle-assets

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

_Made as a copy of https://pages.gitlab.io/plain-html/ example._